import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  constructor(
    private employeeService: EmployeeService,
    private router: Router
  ) { }

  employee: any = {};

  ngOnInit(): void {
  }

  addEmployee(){
    this.employeeService.postEmployee(this.employee).subscribe((res: any) => {
      this.router.navigate(['employee']);
    })
  }

}
