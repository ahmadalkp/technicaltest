import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private httpClient: HttpClient,
  ) {  }

  url = "http://localhost:8080/employees";

  getEmployeeList(): Observable<any> {
    return this.httpClient.get<any>(this.url);
  }

  getEmployeeById(id: any): Observable<any> {
    return this.httpClient.get<any>(this.url + "/" + id)
  }

  postEmployee(bodyrequest: any) {
    return this.httpClient.post(this.url, bodyrequest);
  }

  transformDate(param: any) {    
    const date = new Date(param);
    const newDate = date.toLocaleDateString("en-US")
    
    return newDate;
  }
}
