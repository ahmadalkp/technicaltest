import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  constructor(
    private employeeService: EmployeeService,
    private router: ActivatedRoute
  ) { }

  Id = this.router.snapshot.paramMap.get('id')
  employee: any = {};

  ngOnInit(): void {
    this.employeeService.getEmployeeById(this.Id).subscribe((res: any) => {
      this.employee = res;
    })
  }

  transformDate(param: any) {   
    return this.employeeService.transformDate(param);
  }
}
