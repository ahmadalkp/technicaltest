import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './services/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
  ) { }

  employees: any = [
    {
      "id": 153,
      "username": "username",
      "firstName": "first",
      "lastName": "last",
      "email": "first.last@gmail.com",
      "birthDate": "2024-06-12T17:00:00.000+00:00",
      "basicSalary": null,
      "status": "good",
      "groups": null,
      "description": "2024-06-26T17:00:00.000+00:00"
    },
    {
      "id": 154,
      "username": "goerge",
      "firstName": "goerge",
      "lastName": "bush",
      "email": "goergebush@gmail.com",
      "birthDate": "2003-02-03T17:00:00.000+00:00",
      "basicSalary": 200000,
      "status": "good",
      "groups": "3",
      "description": "2012-12-30T17:00:00.000+00:00"
    }
  ]

  ngOnInit(): void {
    this.employeeService.getEmployeeList().subscribe((res: any) => {
      this.employees = res.data;
    })
  }

  goToDetail(param: any) {
    this.router.navigate(["detailEmployee/" + param]);
  }

  transformDate(param: any) {
    return this.employeeService.transformDate(param);
  }


}
