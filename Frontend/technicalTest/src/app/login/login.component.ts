import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    public router: Router,
  ) { }

  dataProperties: any = {};
  username = "user";
  password = "admin";

  ngOnInit(): void {
  }

  cekLogin() {
    console.log(this.dataProperties);
    
    if (this.dataProperties.username == this.username && this.dataProperties.password == this.password) {
      this.router.navigate(['employee'])
    } else {
      this.dataProperties.error = true;
    }
  }

}
