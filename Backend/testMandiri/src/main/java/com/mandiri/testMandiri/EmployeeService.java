package com.mandiri.testMandiri;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getEmployeeData() {
        List<Employee> employees = employeeRepository.findAll();
        return employees;
    }

    public Employee getEmployeeDataById(Long id) {
        Employee employee = new Employee();
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if(employeeOptional.isPresent()){
            employee = employeeOptional.get();
        }
        return employee;
    }

    public void insertEmployeeData(Employee employee) {
        employeeRepository.save(employee);
    }

    public void updateEmployeeData(Employee employee) {
        employeeRepository.save(employee);
    }

    public void deleteEmployeeData(Long id) {
        employeeRepository.deleteById(id);
    }

}
