package com.mandiri.testMandiri;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Table(name = "employee")
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="username")
    private String username;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="email")
    private String email;

    @Column(name="birth_date")
    private Date birthDate;

    @Column(name="basic_salary")
    private Integer basicSalary;

    @Column(name="status")
    private String status;

    @Column(name="groups")
    private String groups;

    @Column(name="description")
    private Date description;

}
