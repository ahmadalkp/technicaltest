package com.mandiri.testMandiri;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/employees")
@CrossOrigin
@RestController
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping()
    public ResponseEntity<JSONObject> getEmployees(){

        JSONObject jsonResponse = new JSONObject();

        List<Employee> employeeList = employeeService.getEmployeeData();
        jsonResponse.put("data", employeeList);
        return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable(value = "id") Long id){
        return employeeService.getEmployeeDataById(id);
    }

    @PostMapping()
    public void postEmployee(@RequestBody Employee employee){
        employeeService.insertEmployeeData(employee);
    }

    @PutMapping("/{id}")
    public void updateEmployee(@PathVariable("id") Long id, @RequestBody Employee employee){
        Employee oldEmployee = employeeService.getEmployeeDataById(id);

        Employee newEmployee = new Employee();

        newEmployee.setId(oldEmployee.getId());
        newEmployee.setUsername(employee.getUsername());
        newEmployee.setFirstName(employee.getFirstName());
        newEmployee.setLastName(employee.getLastName());
        newEmployee.setEmail(employee.getEmail());
        newEmployee.setBirthDate(employee.getBirthDate());
        newEmployee.setBasicSalary(employee.getBasicSalary());
        newEmployee.setStatus(employee.getStatus());
        newEmployee.setGroups(employee.getGroups());
        newEmployee.setDescription(employee.getDescription());

        employeeService.updateEmployeeData(newEmployee);
    }

    @DeleteMapping("/{id}")
    public void deleteEmployeeById(@PathVariable(value = "id") Long id){
        employeeService.deleteEmployeeData(id);
    }
}
