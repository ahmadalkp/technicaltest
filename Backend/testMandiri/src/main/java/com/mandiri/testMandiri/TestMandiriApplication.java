package com.mandiri.testMandiri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class TestMandiriApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestMandiriApplication.class, args);
	}

}